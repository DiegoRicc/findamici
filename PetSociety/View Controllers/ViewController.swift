//
//  ViewController.swift
//  PierDiego
//
//  Created by Diego Riccardi on 07/05/2020.
//  Copyright © 2020 Diego Riccardi. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    
    @IBOutlet weak var serchBar: UISearchBar!
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var filtri: [Filters] = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.view.backgroundColor = UIColor(patternImage: (#imageLiteral(resourceName: "orange")))
        
        
        self.filtri = [Filters.init(pic: #imageLiteral(resourceName: "Vector1"), titolo: "FILTERS", identif: 1), Filters.init(pic: #imageLiteral(resourceName: "Vector2"), titolo: "SHELTER", identif: 2), Filters.init(pic: #imageLiteral(resourceName: "Vector3"), titolo: "PRIVATE", identif: 3), Filters.init(pic: #imageLiteral(resourceName: "Vector5"), titolo: "CATS", identif: 4), Filters.init(pic: #imageLiteral(resourceName: "Vector4"), titolo: "DOGS", identif: 5), Filters.init(pic: #imageLiteral(resourceName: "Vector6"), titolo: "MALE", identif: 6) , Filters.init(pic: #imageLiteral(resourceName: "Vector7"), titolo: "FEMALE", identif: 7)]
        
        serchBar.backgroundColor = .clear
        
        
    }


    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filtri.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let filtro = self.filtri[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FiltersCollectionViewCell
        
        cell.filterImage.image = filtro.pic
        cell.filterLabel.text = filtro.titolo
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedData = filtri[indexPath.row]
        print(selectedData)
        if selectedData.titolo == "FILTERS" {
            performSegue(withIdentifier: "segue", sender: selectedData)
        
            
        }else if let cell = collectionView.cellForItem(at: indexPath) as? FiltersCollectionViewCell{
            cell.showIcon(sendr: cell)
        }
    }
   
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? FiltersCollectionViewCell{
            cell.hideIcon(sendr: cell)
        }
        
    }
    
    
    
    
}

