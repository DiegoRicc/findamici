//
//  funcolor.swift
//  PierDiego
//
//  Created by Diego Riccardi on 07/05/2020.
//  Copyright © 2020 Diego Riccardi. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func setGradientColor(colorOne: UIColor, colorTwo: UIColor)  {
        let layer = CAGradientLayer()
        layer.frame = bounds
        layer.colors = [colorOne.cgColor, colorTwo.cgColor]
        layer.startPoint = CGPoint (x: 0.5,y: 0)
        layer.endPoint = CGPoint (x: 0.5,y: 1)
        layer.addSublayer(layer)
    }
    
   
}
