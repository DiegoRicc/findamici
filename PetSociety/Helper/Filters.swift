//
//  Filters.swift
//  PierDiego
//
//  Created by Diego Riccardi on 07/05/2020.
//  Copyright © 2020 Diego Riccardi. All rights reserved.
//

import Foundation
import UIKit

struct Filters {
    var pic: UIImage
    var titolo: String
    var identif: Int
}
